## Run via cloud-init

```shell
#!/usr/bin/env -S -- sh -c 'curl -fL https://gitlab.com/emulation-as-a-service/experiments/eaas-ci-test/-/raw/master/user-data | sh'
```

### Access from remote

```shell
ssh -L 8080:localhost:8080 -l ubuntu 
```
