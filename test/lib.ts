import puppeteer from "puppeteer";

import * as child_process from "child_process";
import { promisify } from "util";
const execFile = promisify(child_process.execFile);

type stringOrLocalName = string | { localName: string };

const xpathString = (string: string) =>
    `concat("","${string.replace(/"/g, '",\'"\',"')}")`;

const startsWithI = (expr: string, string: string) =>
    `starts-with(translate(normalize-space(${expr}),
    ${xpathString(string.toUpperCase())},
    ${xpathString(string.toLowerCase())}),
    ${xpathString(string.toLowerCase())})`;

const xpathStartsWithI = (string: stringOrLocalName) =>
    typeof string === "string"
        ? `//node()[self::text() and ${startsWithI(
              ".",
              string,
          )} or ${startsWithI("@placeholder", string)} or ${startsWithI(
              "@value",
              string,
          )}]/parent::*[name(.)!="script" and name(.)!="style" and name(.)!="title"]`
        : `//descendant-or-self::*[name(.)=${xpathString(string.localName)}]`;

const xpathStartsWithIAfter = (string: stringOrLocalName, before: string) =>
    `${xpathStartsWithI(before)}/following::*${xpathStartsWithI(string)}`;

const xpathStartsWithIBefore = (string: stringOrLocalName, after: string) =>
    `(${xpathStartsWithI(after)}/preceding::*${xpathStartsWithI(string)})
      [position()=last()]`;

const evaluate = function*(expr: string) {
    const it = document.evaluate(expr, document);
    for (let val; (val = it.iterateNext()); yield val);
};

export const timeout = (time_ms: number) =>
    new Promise(r => setTimeout(r, time_ms));

// [...document.querySelectorAll("a, button")].filter(v => v.innerText.trim().toLowerCase().startsWith(string.toLowerCase()) && v.getBoundingClientRect().width !== 0)

export class Page2 {
    constructor(public page: puppeteer.Page) {}
    async waitForString(string: stringOrLocalName) {
        return this.page.waitForXPath(xpathStartsWithI(string));
    }
    async waitForStringAfter(string: stringOrLocalName, before: string) {
        return this.page.waitForXPath(xpathStartsWithIAfter(string, before));
    }
    async waitForStringBefore(string: stringOrLocalName, after: string) {
        return this.page.waitForXPath(xpathStartsWithIBefore(string, after));
    }
    async click(
        string: stringOrLocalName,
        {
            before,
            after,
        }: { before?: string; after?: string; localName?: string } = {},
    ) {
        const obj = await (after
            ? this.waitForStringAfter(string, after)
            : before
            ? this.waitForStringBefore(string, before)
            : this.waitForString(string));
        console.log(await obj.evaluate(v => v.outerHTML));
        obj.evaluate(v => ((v as HTMLElement).style.outline = "5px red solid"));
        await timeout(1000);
        obj.evaluate(v => ((v as HTMLElement).style.outline = "unset"));
        await obj.click();
    }
    async type(string: string) {
        await this.page.keyboard.type(string);
    }
    async clickUntilGone(string: string) {
        try {
            for (;;) await this.click(string);
        } catch {}
    }
}

export const run = (command: string, args: string[] = []) => {
    const process = child_process.spawn(command, args, {
        stdio: "inherit",
    });
    return new Promise((resolve, reject) => {
        process.on("error", reject);
        process.on("exit", resolve);
    });
};
