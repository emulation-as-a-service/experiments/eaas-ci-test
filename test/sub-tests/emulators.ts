
import { Page2, run, timeout } from "../lib.js";

export const run_emulator_import = async (page: Page2, _url: URL) => {
   await _import(page, _url, "eaas/qemu-eaas", "latest");
   await _import(page, _url, "eaas/qemu-eaas", "v2-5deb");
   await _import(page, _url, "registry.gitlab.com/emulation-as-a-service/emulators/browser", "chrome-53");
   await _import(page, _url, "registry.gitlab.com/emulation-as-a-service/emulators/macemu-eaas-sheepshaver", "latest");
   await _import(page, _url, "registry.gitlab.com/emulation-as-a-service/emulators/macemu-eaas-basilisk2", "latest");    
};


const _import = async (page: Page2, _url: URL, ref: string, tag: string) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    await timeout(5_000);
    await page.click("Administration");
    await timeout(1_000);
    await page.click("Emulators");
    await timeout(1_000);
    await page.click("Import Emulator");
    await timeout(1_000);
    await page.click("Name:");
    await page.type(ref);

    await page.click("Tag")
    await page.type(tag);
    await page.click("OK");

    await page.clickUntilGone("importing image");
    await timeout(5_000);
    await page.page.screenshot({ path: "./artifacts/emulators-import.png" });
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    return;
};