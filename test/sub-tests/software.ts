import { Page2, run, timeout } from "../lib.js";

export const run_software_delete = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    await timeout(1_000);

    await page.click("Software");
    await page.click("Choose action");
    await page.click("delete");
    await page.type(String.fromCharCode(13));
}
