import { Page2, run, timeout } from "../lib.js";

export const run_images_import = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    await timeout(5_000);
    await page.click("Images");
    await timeout(1_000);
    await page.click("New Image");
    await timeout(1_000);
    await page.click("Disk image label");
    await page.type("imported test image");
    await page.click("Import image");
    await page.click("Import Image from URL");
    await page.type("https://people.debian.org/~aurel32/qemu/amd64/debian_squeeze_amd64_standard.qcow2");
    await page.click("OK");
    await page.clickUntilGone("Please wait");
    await timeout(1_000);
    await page.page.screenshot({ path: "./artifacts/image-imported.png" });
    return;
}
export const run_images_delete = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    await timeout(5_000);
    await page.click("Images");
    await timeout(1_000);
    await page.click("Choose action");
    await page.click("delete");
    await page.click("ok");
    await page.page.screenshot({ path: "./artifacts/image-deleted.png" });
}