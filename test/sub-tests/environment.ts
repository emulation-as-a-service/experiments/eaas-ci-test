import { Page2, run, timeout } from "../lib.js";


export const run_environment_create = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });

    await timeout(1_000);
    await page.click("Environments");
    await timeout(5_000);
    await page.click("Create Environment");
    await timeout(1_000);
    await page.click("Linux");

    await page.click("Machine name");
    await page.type("test environment");
   
    await page.click("Search or choose");
    await page.click("Debian/Ubuntu");
    await page.page.click("body > div.ng-scope > div.main-container.container.ng-scope > div:nth-child(2) > div.col-sm-9 > div > form > div.panel.panel-default.ng-scope.ng-enter-prepare > div.panel.panel-default > div > div:nth-child(5) > div > drives-overview > ul > li:nth-child(3) > div.btn.btn-default");
    await timeout(1_000);
    await page.click("Select from disk image library");
    await page.click("Select disk image");
    await page.click("imported test image");
    await page.click("ok");
    await page.click("save");
}

export const run_environment_create_browser = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });

    await timeout(1_000);
    await page.click("Environments");
    await timeout(5_000);
    await page.click("Create Environment");
    await timeout(1_000);
    await page.click("Other");

    await page.click("Machine name");
    await page.type("browser");
   
    await page.click("Search or choose");
    await page.click("Browser Chrome");
   
    await page.click("save");
}

export const run_environment_enable_network = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    await page.click("Environments");
    await timeout(1_000);
    await page.click("Search");
    await page.type("test environment");

    await page.click("Choose action");
    await page.click("details");
    await timeout(2_000);
    await page.click("Enable networking");

    await timeout(2_000);
    await page.click("save");
}

export const run_environment_start = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });

    await timeout(1_000);
    await page.click("Environments");
    await timeout(1_000);
    await page.click("Choose Action");
    await page.click("Run Environment");

    await timeout(30_000);
    await page.page.screenshot({ path: "./artifacts/environment-run-test.png" });
    await page.click("Stop");
    await timeout(1_000);
    await page.click("OK");
}

export const run_environment_save_derivate = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });

    await timeout(1_000);
    await page.click("Environments");
    await timeout(1_000);

    await page.click("Search");
    await page.type("test");

    await page.click("Choose Action");
    await page.click("Run Environment");

    await timeout(30_000);
    await page.click("Save Environment");
    await timeout(1_000);
    await page.click("OK");

    await page.click("Description of changes");
    await page.type("testing");

    await timeout(1_000);
    await page.click("OK");
}

export const run_environment_save_checkpoint = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });

    await timeout(1_000);
    await page.click("Environments");
    await timeout(1_000);

    await page.click("Search");
    await page.type("test");

    await page.click("Choose Action");
    await page.click("Run Environment");

    await timeout(30_000);
    await page.click("Snapshot");
    await timeout(1_000);
    await page.click("OK");

    await page.clickUntilGone("Creating checkpoint. This might take some time.");
    await timeout(1_000);

    await page.click("run");
    await timeout(30_000);
    await page.page.screenshot({ path: "./artifacts/environment-run-checkpoint.png" });
    await page.click("Stop");
}

export const run_environment_delete = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });

    await timeout(1_000);
    await page.click("Environments");
    await timeout(1_000);
    await page.click("Choose Action");
    await page.click("Delete");
    await page.click("Ok");
}