import { Page2, run, timeout } from "../lib.js";

export const run_create_runtime = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    await timeout(10_000);
    await page.click("Administration");
    await timeout(1_000);
    await page.click("Container Runtimes");
    await timeout(1_000);
    await page.click("Create Runtime");
    await timeout(1_000);
    await page.click("Name");
    await page.type("test runtime");

    await page.click("DNS / DHCP Service");
    await page.click("User Storage (SMB)");
    await page.click("Linux Archives Proxy Service");

    await page.click("OK");
    await page.clickUntilGone("Creating runtime");
    await page.clickUntilGone("Importing service container");
    await page.page.screenshot({ path: "./artifacts/runtime-created.png" });
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    return;
}