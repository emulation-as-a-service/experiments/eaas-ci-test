import { Page2, run, timeout } from "../lib.js";

export const run_network_delete = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });

    await timeout(5_000);
    await page.click("Networks");
    await timeout(1_000);

    await page.click("search");
    await page.type("test network");
    await timeout(1_000);

    await page.click("Choose action");
    await page.click("delete");
    await page.click("ok");
}

export const run_network_create_slirp = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    await timeout(5_000);
    await page.click("Networks");
    await timeout(1_000);
    await page.click("Create Network");
    await page.click("Network Environment Title");
    await page.type("test network simple");
    await page.click("Network (e.g.");
    await page.type("10.0.0.0/24");
    await page.click("Enable Internet access");
    await timeout(1_000);
    await page.click("Network Gateway");
    await page.type("10.0.0.1");
    await page.click("enable slirp");

    await page.click("environments with network");
    await page.click("test environment");
    await page.click("environment label");
    await page.type("test net env");
    await page.page.click("[id='_add_environment']");
    await page.page.click("[id='_save']");
}

export const run_network_create_dhcp_internet = async (page: Page2, _url: URL) => {
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    await timeout(5_000);
    await page.click("Networks");
    await timeout(1_000);
    await page.click("Create Network");
    await page.click("Network Environment Title");
    await page.type("test network simple");
    await page.click("Network (e.g.");
    await page.type("10.0.0.0/24");
    await page.click("Enable Internet access");
    await timeout(1_000);
    await page.click("Network Gateway");
    await page.type("10.0.0.1");
    
    await page.click("DNS/DHCP Service");
    await page.click("DHCP Server IP");
    await page.type("10.0.0.2");

    await page.click("environments with network");
    await page.click("test environment");
    await page.click("environment label");
    await page.type("test net env");
    await page.page.click("[id='_add_environment']");
    await page.page.click("[id='_save']");
}
