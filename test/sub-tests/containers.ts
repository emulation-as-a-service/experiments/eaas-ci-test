import { Page2, run, timeout } from "../lib.js";

export const run_container_import = async (page: Page2, _url: URL) => {
    
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    await timeout(1_000);
    await page.click("Environments");
    await timeout(1_000);
 //    await timeout(20_000);
    await page.click("Containers");
    await timeout(1_000);
    await page.click("Import Container");
    await page.click("Choose a source");
    await page.click("docker");
    await page.click("name:");
    await page.type("busybox");
    await page.click("tag");
    await page.type("latest");

    await page.click("start");
    await timeout(1_000);
    await page.clickUntilGone("Preparing container");
    await timeout(1_000);

    await page.click("Title");
    await page.type("test container import");

    await page.click("select runtime");
    await page.click("test runtime");

    await page.click("Author")
    await page.type("openslx");

    await page.click("OK");
    await page.clickUntilGone("Importing container as EaaS image");
    await page.page.screenshot({ path: "./artifacts/container-created.png" });
    await page.page.goto(String(_url), { waitUntil: "networkidle2" });
    return;

}