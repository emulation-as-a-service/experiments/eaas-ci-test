import { run, timeout } from "./lib.js";
import { promises as fs } from "fs";
const { stat } = fs;

const EAAS_PROXY_PATH = "./eaas-proxy";
const EAAS_PROXY_URL =
    "https://gitlab.com/emulation-as-a-service/eaas-proxy/-/jobs/artifacts/master/raw/eaas-proxy/eaas-proxy?job=build";

const eaasClient = await page.page.evaluateHandle(
    () =>
        angular.element(document.querySelector("[ui-view=wizard]")).scope()
            .startEmuCtrl.eaasClient,
);

console.log(eaasClient);
await page.click("Network Environment Overview");

const proxyURLTelnet = await page.page.evaluate(eaasClient => {
    return eaasClient.getProxyURL({
        serverIP: "10.0.0.1",
        serverPort: "23",
        localPort: "8099",
    });
}, eaasClient);
console.log("telnet is available at:\n", proxyURLTelnet);
const proxyURL = await page.page.evaluate(eaasClient => {
    return eaasClient.getProxyURL({
        serverIP: "socks5",
        serverPort: "",
        localPort: "8090",
    });
}, eaasClient);
console.log(proxyURL);
if (!(await stat(EAAS_PROXY_PATH).catch(() => {}))) {
    await run("curl", ["-L", "-o", EAAS_PROXY_PATH, EAAS_PROXY_URL]);
    await run("chmod", ["+x", EAAS_PROXY_PATH]);
}
await run(EAAS_PROXY_PATH, [proxyURL]);
