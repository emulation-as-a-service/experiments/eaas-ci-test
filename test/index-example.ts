#!/bin/sh
true /*; exec node --loader ts-node/esm.mjs "$0" "$@";*/;

import { Page2, run, timeout } from "./lib.js";
import { run_create_runtime } from "./sub-tests/runtime.js";
import { run_container_import } from "./sub-tests/containers.js";
import { run_emulator_import } from "./sub-tests/emulators.js";
import {
    run_environment_create, 
    run_environment_start, 
    run_environment_save_derivate,
    run_environment_delete,
    run_environment_enable_network,
    run_environment_create_browser
} from "./sub-tests/environment.js";
import { run_images_import, run_images_delete } from "./sub-tests/images.js";

import { 
    run_network_create_slirp, 
    run_network_delete, 
    run_network_create_dhcp_internet 
} from "./sub-tests/networks.js";

import * as process from "process";
const [_url, command] = process.argv.slice(2);
const url = new URL(_url);
const { username, password } = url;

import puppeteer from "puppeteer";

declare const angular: any;

(async () => {
    const browser = await puppeteer.launch({
        headless: false,
        args: ['--lang=en-US;en']
    });
    const page = new Page2(await browser.newPage());
    (globalThis as any).page = page;

    await run_emulator_import(page, url);
    await run_images_import(page, url);
    //await run_create_runtime(page, url);
    await run_environment_create(page, url);
    await run_environment_create_browser(page, url);
    // await run_environment_enable_network(page, url);
   // await run_network_create_dhcp_internet(page, url);
    await run_environment_start(page, url);
    await run_environment_save_derivate(page, url);
    
    /*
    
    await run_network_delete(page, url);
    await run_environment_delete(page, url);
    await run_images_delete(page, url);
    await run_container_import(page, url);
    
    // cleanup
    
    await browser.close();
    */
    return;
    try {
        await page.click("log in");
        await page.click("username");
        await page.type(username);
        await page.click("password");
        await page.type(password);
        await page.click("log in", { after: "password" });
    } catch {}

    if (command === "import") {
        await page.click("import container");
        await page.click("choose a runtime");
        await page.click("docker");
        await page.click("name:");
        await page.type(
            "registry.gitlab.com/emulation-as-a-service/experiments/fake-dns",
        );
        await page.click("image tag");
        await page.type("debug-proxy-ia");
        await page.click("start");
        await page.clickUntilGone("importing image");
        await page.click({ localName: "input" }, { after: "title" });
        await page.type(`pywb ${new Date().toISOString()}`);
        await page.click({ localName: "p" }, { after: "description" });
        await page.type(".");
        await page.click("ok");
        return;
    }
    // await page.click("machines");
    // await page.click("virtual machines");
    await page.click("environments");
    //await page.click("networks");
    await page.click("search");
    const envName = "tracenoizer";
    await page.type(envName);
    await page.click("choose action");
    await page.click("run");
    await page.click("proceed without connecting");
    await timeout(1000);
    console.log("RUNNING");
    console.log("DONE");
})()
    .catch(e => {
        console.error(e);
        throw e;
    })
    .finally(async () => {
        (await import("repl")).start();
    });
